-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: proteczac
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `colonia`
--

USE proteczac;

DROP TABLE IF EXISTS `colonia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `colonia` (
  `id_colonia` int(11) NOT NULL DEFAULT '0',
  `nombre_colonia` varchar(50) NOT NULL,
  `c_postal` char(5) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `municipio` int(3) DEFAULT NULL,
  PRIMARY KEY (`id_colonia`),
  KEY `fk_municipio_colonia_idx` (`municipio`),
  CONSTRAINT `fk_municipio_colonia` FOREIGN KEY (`municipio`) REFERENCES `municipio` (`id_municipio`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colonia`
--

LOCK TABLES `colonia` WRITE;
/*!40000 ALTER TABLE `colonia` DISABLE KEYS */;
INSERT INTO `colonia` VALUES (1,'21 de Julio','98077','Colonia',56),(2,'3 Cruces Infonavit','98064','Unidad habitacional',56),(3,'5 Señores','98089','Fraccionamiento',56),(4,'Agronómica II','98066','Colonia',56),(5,'Agrónomo','98060','Colonia',56),(6,'Alma Obrera','98090','Colonia',56),(7,'América','98057','Colonia',56),(8,'Ampliación Gral. Felipe Ángeles','98054','Fraccionamiento',56),(9,'Arquitectos','98087','Fraccionamiento',56),(10,'Arturo Romo Macias','98090','Fraccionamiento',56),(11,'Ayuntamiento','98078','Colonia',56),(12,'Bancomer','98040','Colonia',56),(13,'Barros Sierra','98090','Fraccionamiento',56),(14,'Bellavista','98080','Colonia',56),(15,'Benito Juárez','98080','Colonia',56),(16,'Benito Juárez (san Cayetano)','98186','Poblado comunal',56),(17,'Boulevares','98065','Fraccionamiento',56),(18,'Bracho (Lomas de Bracho)','98023','Colonia',56),(19,'Buenavista','98070','Colonia',56),(20,'Buenos Aires','98056','Colonia',56),(21,'Caminera','98045','Colonia',56),(22,'Camino Real','98085','Colonia',56),(23,'Carlos Hinojosa Petit','98099','Colonia',56),(24,'Casa Blanca','98090','Fraccionamiento',56),(25,'Cerrada del Ángel','98085','Fraccionamiento',56),(26,'Cerro de la Virgen','98085','Fraccionamiento',56),(27,'Cieneguillas','98170','Ranchería',56),(28,'Ciudad Argentum','98047','Fraccionamiento',56),(29,'Ciudad Gobierno','98160','Fraccionamiento',56),(30,'Ciudad Universitaria','98066','Fraccionamiento',56),(31,'Cnop','98053','Colonia',56),(32,'Colinas del Padre','98085','Fraccionamiento',56),(33,'Colinas del Sol','98087','Colonia',56),(34,'Colinas del Vergel','98085','Fraccionamiento',56),(35,'Colonial Zacatecas','98068','Colonia',56),(36,'Conquistadores','98090','Colonia',56),(37,'Constelaciones','98085','Colonia',56),(38,'De Los Taxista','98078','Fraccionamiento',56),(39,'De Los Taxista 2da. Sección','98078','Fraccionamiento',56),(40,'De Olivos','98024','Colonia',56),(41,'Del Bosque','98087','Colonia',56),(42,'DIF','98083','Fraccionamiento',56),(43,'El Chaveño','98085','Colonia',56),(44,'El Diamante','98085','Colonia',56),(45,'El Jaralillo','98085','Colonia',56),(46,'El Maguey','98175','Poblado comunal',56),(47,'El Mirador','98079','Fraccionamiento',56),(48,'El Molino','98183','Poblado comunal',56),(49,'El Orito','98160','Ranchería',56),(50,'El Orito 1a Sección','98087','Fraccionamiento',56),(51,'Emiliano Zapata','98054','Colonia',56),(52,'España','98054','Colonia',56),(53,'Estrella de Oro','98087','Colonia',56),(54,'Europa','98087','Colonia',56),(55,'Ex Hacienda Bernardez','98064','Colonia',56),(56,'Fernando Pamanes Escobedo','98053','Colonia',56),(57,'Fovissste','98064','Unidad habitacional',56),(58,'Francisco E Garcia','98070','Colonia',56),(59,'Francisco I Madero','98176','Poblado comunal',56),(60,'Frente Popular','98046','Colonia',56),(61,'Fuente Del Bosque','98067','Fraccionamiento',56),(62,'Garcia de La Cadena (el Visitador)','98183','Poblado comunal',56),(63,'Garcia Salinas','98018','Colonia',56),(64,'Gonzalez Ortega (Machines)','98180','Ejido',56),(65,'Gonzalo García García','98099','Colonia',56),(66,'Gral. Felipe Angeles','98054','Fraccionamiento',56),(67,'Gustavo Diaz Ordaz 1a Secc','98020','Colonia',56),(68,'Gustavo Diaz Ordaz 2a Secc','98020','Colonia',56),(69,'Gustavo Diaz Ordaz 3a Secc','98020','Colonia',56),(70,'Héroes de Chapultepec','98057','Colonia',56),(71,'Hidráulica','98068','Colonia',56),(72,'Huerta Vieja','98087','Colonia',56),(73,'ISSSTEZAC I','98087','Fraccionamiento',56),(74,'J. Jesús González Ortega','98087','Colonia',56),(75,'Korea I','98087','Colonia',56),(76,'Korea II','98087','Colonia',56),(77,'La Bufa','98069','Colonia',56),(78,'La Encantada','98088','Fraccionamiento',56),(79,'La Escondida','98160','Ejido',56),(80,'La Herradura','98086','Fraccionamiento',56),(81,'La Loma','98068','Fraccionamiento',56),(82,'La Peñuela','98060','Fraccionamiento',56),(83,'La Pimienta','98177','Poblado comunal',56),(84,'La Pinta','98024','Colonia',56),(85,'La Soledad (la Chole)','98185','Poblado comunal',56),(86,'La Toma de Zacatecas','98057','Colonia',56),(87,'Ladrilleras','98054','Fraccionamiento',56),(88,'Las Boquillas','98160','Poblado comunal',56),(89,'Las Chilitas','98184','Poblado comunal',56),(90,'Las Colinas','98098','Fraccionamiento',56),(91,'Las Cumbres','98077','Colonia',56),(92,'Las Flores','98087','Colonia',56),(93,'Las Flores','98099','Colonia',56),(94,'Las Haciendas','98089','Fraccionamiento',56),(95,'Las Huertas','98087','Colonia',56),(96,'Las Mercedes','98024','Colonia',56),(97,'Las Palmas','98056','Colonia',56),(98,'Lázaro Cárdenas','98040','Colonia',56),(99,'Libertadores','98085','Fraccionamiento',56),(100,'Loma Barroca','98040','Fraccionamiento',56),(101,'Loma Dorada','98066','Fraccionamiento',56),(102,'Lomas Bizantinas','98099','Fraccionamiento',56),(103,'Lomas de Cristo','98085','Colonia',56),(104,'Lomas de la Isabelica','98099','Fraccionamiento',56),(105,'Lomas de La Pimienta','98053','Colonia',56),(106,'Lomas de La Soledad','98040','Fraccionamiento',56),(107,'Lomas de la Virgen','98097','Fraccionamiento',56),(108,'Lomas de San Francisco','98067','Colonia',56),(109,'Lomas del Bosque','98066','Fraccionamiento',56),(110,'Lomas Del Campestre','98098','Fraccionamiento',56),(111,'Lomas Del Capulín','98050','Colonia',56),(112,'Lomas del Lago','98085','Fraccionamiento',56),(113,'Lomas del Patrocinio','98060','Fraccionamiento',56),(114,'Los Caleros','98070','Fraccionamiento',56),(115,'Los Filarmónicos','98017','Colonia',56),(116,'Los Periodistas','98090','Fraccionamiento',56),(117,'Luis Donaldo Colosio','98046','Fraccionamiento',56),(118,'Magisterial Jardines del Sol','98087','Colonia',56),(119,'Margaritas','98017','Colonia',56),(120,'Marianita','98060','Colonia',56),(121,'Mecánicos','98057','Colonia',56),(122,'Mecánicos 2a Secc','98057','Colonia',56),(123,'Médicos Veterinarios','98097','Fraccionamiento',56),(124,'Mexicapan','98015','Fraccionamiento',56),(125,'Miguel Hidalgo (san Miguel)','98186','Poblado comunal',56),(126,'Miguel Hidalgo 1ra. Sección','98054','Colonia',56),(127,'Miguel Hidalgo 2da. Sección','98054','Fraccionamiento',56),(128,'Miguel Hidalgo 3ra. Sección','98054','Fraccionamiento',56),(129,'Militar','98065','Colonia',56),(130,'Minera','98050','Colonia',56),(131,'Moradores','98067','Fraccionamiento',56),(132,'Nueva Generación','98087','Colonia',56),(133,'Ojo de Agua de Meléndez (Ojo de Agua)','98085','Colonia',56),(134,'Palacio de Gobierno Del Estado de Zacatecas','98009','Gran usuario',56),(135,'Pánfilo Natera','98070','Colonia',56),(136,'Pedro Ruiz González INFONAVIT','98019','Unidad habitacional',56),(137,'Peñas de la Virgen','98085','Colonia',56),(138,'Popular CTM','98099','Fraccionamiento',56),(139,'Primavera','98060','Colonia',56),(140,'Privada Aziz','98017','Fraccionamiento',56),(141,'Privada del Bosque','98067','Fraccionamiento',56),(142,'Privada Residencial San Pedro','98017','Fraccionamiento',56),(143,'Progreso','98066','Colonia',56),(144,'Ramón López Velarde','98010','Colonia',56),(145,'Residencial Cantera','98057','Fraccionamiento',56),(146,'Residencial Del Valle','98068','Fraccionamiento',56),(147,'Residencial La Joya','98097','Fraccionamiento',56),(148,'Ricardo Flores Magón','98070','Colonia',56),(149,'Rinconada de La Isabelica','98099','Fraccionamiento',56),(150,'San Antonio de los Negros (los Negros)','98174','Poblado comunal',56),(151,'San Fernando','98057','Fraccionamiento',56),(152,'San Francisco de Herrera','98067','Fraccionamiento',56),(153,'San Francisco de la Montaña','98067','Fraccionamiento',56),(154,'San Gabriel','98057','Colonia',56),(155,'Santos Bañuelos','98084','Fraccionamiento',56),(156,'Secretaria de Hacienda y Crédito Publico','98008','Gran usuario',56),(157,'Sierra de Alica','98050','Colonia',56),(158,'Siglo XXI','98060','Fraccionamiento',56),(159,'Suave Patria','98085','Colonia',56),(160,'Tahona','98097','Fraccionamiento',56),(161,'Tecnológica','98099','Colonia',56),(162,'Universitaria','98068','Colonia',56),(163,'Úrsulo A. García','98050','Colonia',56),(164,'Villa Antigua','98068','Fraccionamiento',56),(165,'Villa Verde','98098','Fraccionamiento',56),(166,'Villas del Padre','98085','Fraccionamiento',56),(167,'Villas Del Sol','98067','Fraccionamiento',56),(168,'Villas Del Tepozan','98060','Fraccionamiento',56),(169,'Villas Universidad','98160','Fraccionamiento',56),(170,'Zacatecas Centro','98000','Colonia',56),(171,'Zacatlán','98057','Colonia',56),(172,'1910','98600','Fraccionamiento',17),(173,'África','98615','Colonia',17),(174,'Américas 4','98612','Fraccionamiento',17),(175,'Ampliación 2da Sección Tierra y Libertad','98615','Colonia',17),(176,'Ampliación Américas','98612','Fraccionamiento',17),(177,'Ampliación Arboledas','98608','Fraccionamiento',17),(178,'Ampliación Campesina','98605','Colonia',17),(179,'Ampliación La fe','98615','Fraccionamiento',17),(180,'Ampliación Minas','98615','Colonia',17),(181,'Arcoiris','98605','Colonia',17),(182,'Barrio de Los Moros','98607','Colonia',17),(183,'Bellavista','98607','Colonia',17),(184,'Bonaterra','98609','Fraccionamiento',17),(185,'Bonito Pueblo','98613','Fraccionamiento',17),(186,'Brisas del Campo','98612','Fraccionamiento',17),(187,'California','98616','Fraccionamiento',17),(188,'California II','98616','Fraccionamiento',17),(189,'Camilo Torres','98619','Colonia',17),(190,'Camino Real','98613','Colonia',17),(191,'Campestre San José','98606','Fraccionamiento',17),(192,'Campo Bravo','98608','Fraccionamiento',17),(193,'Campo Real','98612','Fraccionamiento',17),(194,'Cañada de La Bufa','98619','Fraccionamiento',17),(195,'Cañada de la Laguna II','98604','Fraccionamiento',17),(196,'Cañada del Sol','98619','Colonia',17),(197,'Casa Blanca','98620','Ejido',17),(198,'Cieneguitas','98658','Pueblo',17),(199,'Complejo de Naves Industriales la Zacatecana','98604','Zona industrial',17),(200,'Conde de Bernardez','98617','Fraccionamiento',17),(201,'Conquistadores','98605','Fraccionamiento',17),(202,'Culturas','98612','Fraccionamiento',17),(203,'De la Estación','98607','Barrio',17),(204,'De Loreto','98607','Barrio',17),(205,'Del Agua','98612','Fraccionamiento',17),(206,'Del Carmen','98608','Unidad habitacional',17),(207,'Del Parque','98608','Fraccionamiento',17),(208,'Dependencias Federales','98618','Fraccionamiento',17),(209,'División del Norte','98605','Colonia',17),(210,'Doroteo Arango','98605','Colonia',17),(211,'Ejidal','98613','Colonia',17),(212,'El Bordo','98621','Ranchería',17),(213,'El Dorado','98617','Colonia',17),(214,'El Hípico','98605','Fraccionamiento',17),(215,'El Maguey','98630','Colonia',17),(216,'El Mezquital','98606','Fraccionamiento',17),(217,'El Montecito','98605','Colonia',17),(218,'El Paraíso','98613','Fraccionamiento',17),(219,'El Pescado','98655','Ranchería',17),(220,'El Pirular','98613','Fraccionamiento',17),(221,'El Rosal','98605','Colonia',17),(222,'El Salero','98607','Fraccionamiento',17),(223,'El Triángulo','98612','Fraccionamiento',17),(224,'El Universo','98612','Colonia',17),(225,'Emiliano Zapata','98615','Colonia',17),(226,'Escritores','98606','Fraccionamiento',17),(227,'Ex Hacienda de Bernardez','98619','Colonia',17),(228,'Ferrocarrileros','98607','Fraccionamiento',17),(229,'FIRCO','98615','Fraccionamiento',17),(230,'FOVISSSTE','98612','Colonia',17),(231,'Francisco E. García','98620','Ranchería',17),(232,'Francisco García Salinas','98608','Fraccionamiento',17),(233,'Francisco Villa','98615','Fraccionamiento',17),(234,'Galerías','98612','Fraccionamiento',17),(235,'Ganaderos','98616','Fraccionamiento',17),(236,'Gardenias','98659','Fraccionamiento',17),(237,'Gavilanes','98619','Colonia',17),(238,'Geranios 2da. Sección','98619','Fraccionamiento',17),(239,'Grupo Tendencia Sindical','98615','Fraccionamiento',17),(240,'Guadalupe','98604','Zona industrial',17),(241,'Guadalupe Centro','98600','Colonia',17),(242,'Guadalupe Colonial','98615','Colonia',17),(243,'Guadalupe Industrial','98605','Fraccionamiento',17),(244,'Guadalupe Moderno','98613','Fraccionamiento',17),(245,'Hacienda Valle Dorado','98658','Fraccionamiento',17),(246,'Hidráulica 13 de Septiembre','98608','Fraccionamiento',17),(247,'Ignacio Allende','98615','Fraccionamiento',17),(248,'INDECO Arroyo de la Plata','98610','Fraccionamiento',17),(249,'Independencia','98612','Fraccionamiento',17),(250,'ISSSTE Zac 3','98619','Fraccionamiento',17),(251,'ISSSTEZAC','98612','Fraccionamiento',17),(252,'Jardines de Bernardez','98610','Fraccionamiento',17),(253,'Jardines de Sauceda','98612','Fraccionamiento',17),(254,'Jardines del Carmen','98608','Fraccionamiento',17),(255,'Jardines del Sol','98606','Fraccionamiento',17),(256,'Jardines del Sol II','98606','Fraccionamiento',17),(257,'Jesús Pérez Cuevas','98615','Colonia',17),(258,'Justo Sierra','98607','Colonia',17),(259,'La Antorcha de Zacatecas, A. C.','98612','Colonia',17),(260,'La Bufa I','98609','Fraccionamiento',17),(261,'La Bufa II','98609','Fraccionamiento',17),(262,'La Campesina','98605','Colonia',17),(263,'La Cantera','98616','Colonia',17),(264,'La Cantera','98606','Fraccionamiento',17),(265,'La Cañada','98617','Fraccionamiento',17),(266,'La Cañada 2a. Etapa','98615','Fraccionamiento',17),(267,'La Cima','98610','Fraccionamiento',17),(268,'La Comarca','98658','Fraccionamiento',17),(269,'La Condesa','98612','Colonia',17),(270,'La Esperanza','98605','Colonia',17),(271,'La Estación','98607','Fraccionamiento',17),(272,'La Florida','98618','Fraccionamiento',17),(273,'La Ladrillera','98604','Colonia',17),(274,'La Luz','98631','Ranchería',17),(275,'La Martinica','98606','Fraccionamiento',17),(276,'La Palma','98606','Colonia',17),(277,'La Providencia','98607','Colonia',17),(278,'La Purísima','98600','Barrio',17),(279,'La Toscana','98610','Fraccionamiento',17),(280,'La Victoria','98614','Colonia',17),(281,'La Villa','98658','Fraccionamiento',17),(282,'La Zacatecana','98659','Pueblo',17),(283,'Las Américas','98612','Fraccionamiento',17),(284,'Las Arboledas','98608','Fraccionamiento',17),(285,'Las Flores','98612','Colonia',17),(286,'Las Flores','98605','Colonia',17),(287,'Las Fuentes','98613','Fraccionamiento',17),(288,'Las Joyas','98613','Colonia',17),(289,'Las Lomas','98615','Fraccionamiento',17),(290,'Las Orquídeas','98612','Fraccionamiento',17),(291,'Las Palmas','98606','Colonia',17),(292,'Las Quintas','98607','Colonia',17),(293,'Las Torres','98612','Fraccionamiento',17),(294,'Lindavista','98600','Colonia',17),(295,'Loma Bonita','98612','Fraccionamiento',17),(296,'Lomas de Bernardez','98610','Fraccionamiento',17),(297,'Lomas de Bernárdez Sección Plata','98610','Fraccionamiento',17),(298,'Lomas de Britania','98610','Fraccionamiento',17),(299,'Lomas de Galicia','98610','Fraccionamiento',17),(300,'Lomas de Guadalupe','98607','Fraccionamiento',17),(301,'Lomas de San Cristóbal','98612','Fraccionamiento',17),(302,'Lomas del Consuelo','98614','Fraccionamiento',17),(303,'Lomas del Convento','98609','Fraccionamiento',17),(304,'Lomas del Pedregal','98610','Fraccionamiento',17),(305,'Lomas del Valle','98615','Colonia',17),(306,'Los Angeles','98607','Colonia',17),(307,'Los Conventos','98612','Fraccionamiento',17),(308,'Los Conventos II','98612','Fraccionamiento',17),(309,'Los Frailes','98605','Colonia',17),(310,'Los Geranios','98619','Fraccionamiento',17),(311,'Los Pirules','98619','Fraccionamiento',17),(312,'Los Prados','98610','Colonia',17),(313,'Los Tepetates','98612','Fraccionamiento',17),(314,'Luis Donaldo Colosio','98615','Colonia',17),(315,'Magisterial Pedro Ruiz González','98612','Fraccionamiento',17),(316,'Magisterio','98609','Fraccionamiento',17),(317,'Manuel M. Ponce','98612','Fraccionamiento',17),(318,'Margaritas','98607','Fraccionamiento',17),(319,'Martínez Domínguez','98659','Pueblo',17),(320,'Mexicana de Aviación','98609','Fraccionamiento',17),(321,'Mezquitillos','98605','Fraccionamiento',17),(322,'Militar','98617','Colonia',17),(323,'Mina Azul','98605','Fraccionamiento',17),(324,'Minas','98617','Fraccionamiento',17),(325,'Minas del Real','98607','Fraccionamiento',17),(326,'Montebello','98612','Fraccionamiento',17),(327,'Montes Azules','98612','Fraccionamiento',17),(328,'Nueva Generación','98605','Fraccionamiento',17),(329,'Nuevo Bernardez','98610','Fraccionamiento',17),(330,'Nuevo Mercurio','98604','Fraccionamiento',17),(331,'Octavio Paz','98605','Fraccionamiento',17),(332,'Ojo de Agua de La Palma','98606','Fraccionamiento',17),(333,'Olimpia','98613','Fraccionamiento',17),(334,'Paseo Real','98604','Fraccionamiento',17),(335,'Popular del Bosque','98615','Fraccionamiento',17),(336,'Popular San Francisco','98613','Colonia',17),(337,'Primero de Mayo','98614','Colonia',17),(338,'Privada Concordia','98612','Fraccionamiento',17),(339,'Privada Conde Santiago de la Laguna','98610','Fraccionamiento',17),(340,'Privada de las Begonias','98614','Fraccionamiento',17),(341,'Privada de Los Olivos','98612','Fraccionamiento',17),(342,'Privada del Bosque','98610','Fraccionamiento',17),(343,'Privada del Santuario','98600','Fraccionamiento',17),(344,'Privada El Salero','98607','Fraccionamiento',17),(345,'Privada Eucaliptos','98618','Fraccionamiento',17),(346,'Privada La Esmeralda','98613','Fraccionamiento',17),(347,'Privada las Águilas','98615','Fraccionamiento',17),(348,'Privada las Misiones','98607','Fraccionamiento',17),(349,'Privada los Manantiales','98606','Fraccionamiento',17),(350,'Privada los Olivares','98618','Fraccionamiento',17),(351,'Privada Portanova','98610','Fraccionamiento',17),(352,'Privada San Andrés','98612','Fraccionamiento',17),(353,'Privada San Antonio','98610','Fraccionamiento',17),(354,'Privada San Charbel','98612','Fraccionamiento',17),(355,'Privada Santa Lucía','98612','Fraccionamiento',17),(356,'Privada Santa María I y II','98610','Fraccionamiento',17),(357,'Privada Trueno','98609','Fraccionamiento',17),(358,'Progresista','98615','Fraccionamiento',17),(359,'Progreso Nacional','98613','Fraccionamiento',17),(360,'Puerta Dorada','98609','Fraccionamiento',17),(361,'Quinta San Fernando','98612','Fraccionamiento',17),(362,'Quinta San Gabriel','98612','Fraccionamiento',17),(363,'Quinta San Jerónimo','98612','Fraccionamiento',17),(364,'Quinta San Nicolás','98612','Fraccionamiento',17),(365,'Quinta San Rafael','98612','Fraccionamiento',17),(366,'Quinta Santa Ana','98612','Fraccionamiento',17),(367,'Quinta Santa Bárbara','98612','Fraccionamiento',17),(368,'Quinta Santa María','98612','Fraccionamiento',17),(369,'Quinta Santa Martha','98612','Fraccionamiento',17),(370,'Quinta Santa Teresita','98612','Fraccionamiento',17),(371,'Ramón López Velarde','98600','Fraccionamiento',17),(372,'Real de Guadalupe','98612','Fraccionamiento',17),(373,'Real de San Gabriel','98605','Fraccionamiento',17),(374,'Real de San Ramón','98605','Fraccionamiento',17),(375,'Real del Conde','98612','Colonia',17),(376,'Real del Sol','98612','Fraccionamiento',17),(377,'Revolución Mexicana','98613','Fraccionamiento',17),(378,'Rincón Colonial','98616','Fraccionamiento',17),(379,'Rincón Guadalupano','98607','Fraccionamiento',17),(380,'Rinconada de los Pinos','98619','Fraccionamiento',17),(381,'Rinconada de Pirules','98619','Fraccionamiento',17),(382,'Rinconada del Edén','98612','Fraccionamiento',17),(383,'S.P.A.U.A.Z.','98613','Fraccionamiento',17),(384,'S.T.U.A.Z.','98609','Fraccionamiento',17),(385,'S.T.U.A.Z.','98613','Fraccionamiento',17),(386,'San Agustín','98612','Fraccionamiento',17),(387,'San Cosme','98612','Fraccionamiento',17),(388,'San Isidro','98613','Fraccionamiento',17),(389,'San Jerónimo','98650','Pueblo',17),(390,'San José (Ampliación Lo de Vega)','98605','Colonia',17),(391,'San Jose de la Piedrera','98607','Barrio',17),(392,'San Miguel del Cortijo','98615','Fraccionamiento',17),(393,'San Ramón','98606','Fraccionamiento',17),(394,'Santa Cecilia','98652','Rancho',17),(395,'Santa Fe','98606','Colonia',17),(396,'Santa Mónica','98636','Poblado comunal',17),(397,'Santa Rita','98610','Fraccionamiento',17),(398,'Santa Rita','98600','Barrio',17),(399,'Sección P.P.S. (Polígono la Fé)','98615','Colonia',17),(400,'Sector Santa Engracia','98609','Fraccionamiento',17),(401,'SNTAS','98608','Fraccionamiento',17),(402,'Solidaridad (José Ives Limantour)','98605','Colonia',17),(403,'SUTSEMOP','98615','Colonia',17),(404,'Tacoaleche','98630','Pueblo',17),(405,'Tierra y Libertad 1','98615','Fraccionamiento',17),(406,'Tierra y Libertad 2','98615','Fraccionamiento',17),(407,'Tierra y Libertad 3ra. Sección','98615','Fraccionamiento',17),(408,'Toma de Zacatecas','98615','Colonia',17),(409,'Tonatiuh Magisterial','98606','Fraccionamiento',17),(410,'U.A.Z.','98616','Fraccionamiento',17),(411,'Unidad Deportiva','98612','Fraccionamiento',17),(412,'Valle Alto','98610','Fraccionamiento',17),(413,'Valle de los Encinos','98604','Fraccionamiento',17),(414,'Valle del Conde','98612','Colonia',17),(415,'Valles I','98613','Colonia',17),(416,'Valles II','98612','Colonia',17),(417,'Villa Colonial','98617','Colonia',17),(418,'Villa de las Flores','98619','Colonia',17),(419,'Villa Real','98612','Fraccionamiento',17),(420,'Villafontana','98612','Fraccionamiento',17),(421,'Villas Bugambilias','98612','Fraccionamiento',17),(422,'Villas de Don Fernando','98613','Colonia',17),(423,'Villas de Guadalupe','98612','Fraccionamiento',17),(424,'Villas de la Coruña','98652','Fraccionamiento',17),(425,'Villas de Nápoles','98606','Colonia',17),(426,'Villas de San Fermín','98612','Fraccionamiento',17),(427,'Villas del Carmen','98608','Fraccionamiento',17),(428,'Villas del Monasterio','98613','Colonia',17),(429,'Villas del Rey','98612','Fraccionamiento',17),(430,'Villas del Tepeyac','98604','Fraccionamiento',17),(431,'Villas Joanna','98612','Fraccionamiento',17),(432,'Villas Mariana','98612','Colonia',17),(433,'Virreyes','98613','Fraccionamiento',17),(434,'Zoquite','98631','Pueblo',17),(435,'Ete',NULL,'Colonia',56);
/*!40000 ALTER TABLE `colonia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `municipio`
--

DROP TABLE IF EXISTS `municipio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `municipio` (
  `id_municipio` int(11) NOT NULL,
  `nombre_mun` varchar(70) NOT NULL,
  PRIMARY KEY (`id_municipio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `municipio`
--

LOCK TABLES `municipio` WRITE;
/*!40000 ALTER TABLE `municipio` DISABLE KEYS */;
INSERT INTO `municipio` VALUES (17,'Guadalupe'),(56,'Zacatecas');
/*!40000 ALTER TABLE `municipio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `peligrosidad_colonias`
--

DROP TABLE IF EXISTS `peligrosidad_colonias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `peligrosidad_colonias` (
  `id_indice` int(11) NOT NULL AUTO_INCREMENT,
  `colonia` int(11) NOT NULL,
  `calificacion` int(2) NOT NULL,
  `periodo` int(1) NOT NULL,
  PRIMARY KEY (`id_indice`),
  KEY `fk_peligrsidad_colonia_idx` (`colonia`),
  KEY `fk_peligrosidad_periodo_idx` (`periodo`),
  CONSTRAINT `fk_peligrosidad_colonia` FOREIGN KEY (`colonia`) REFERENCES `colonia` (`id_colonia`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_peligrosidad_periodo` FOREIGN KEY (`periodo`) REFERENCES `periodo` (`id_periodo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=409 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `peligrosidad_colonias`
--

LOCK TABLES `peligrosidad_colonias` WRITE;
/*!40000 ALTER TABLE `peligrosidad_colonias` DISABLE KEYS */;
INSERT INTO `peligrosidad_colonias` VALUES (1,335,3,1),(2,335,4,3),(3,335,5,4),(4,175,6,3),(5,175,7,4),(6,49,5,3),(7,49,6,4),(8,248,1,1),(9,248,3,2),(10,248,5,3),(11,248,7,4),(12,3,7,3),(13,3,8,4),(14,170,4,2),(15,170,5,3),(16,114,2,1),(17,114,3,2),(18,114,7,3),(19,114,10,4),(20,6,6,2),(21,114,2,1),(22,114,3,2),(23,114,7,3),(24,114,10,4),(25,6,6,2),(26,114,2,1),(27,114,3,2),(28,114,7,3),(29,114,10,4),(30,6,6,2),(31,6,10,3),(32,6,10,4),(33,284,8,3),(34,284,9,4),(35,135,1,1),(36,135,4,2),(37,135,9,3),(38,135,9,4),(39,3,4,1),(40,3,7,2),(41,3,8,3),(42,3,6,4),(43,248,3,4),(44,397,5,3),(45,265,1,4),(46,3,6,2),(47,3,8,3),(48,6,7,2),(49,6,8,3),(50,130,7,2),(51,130,9,3),(52,130,5,2),(53,130,5,3),(54,222,7,2),(55,222,8,3),(56,66,6,3),(57,32,6,3),(58,269,9,3),(59,268,10,3),(60,268,9,4),(61,269,6,1),(62,269,8,3),(63,269,10,4),(64,49,6,1),(65,49,6,2),(66,49,10,3),(67,49,10,4),(68,32,4,4),(69,32,1,2),(70,32,6,3),(71,32,3,4),(72,170,7,3),(73,170,9,4),(74,432,5,3),(75,432,7,4),(76,423,7,3),(77,423,8,4),(78,58,1,1),(79,58,5,2),(80,58,10,3),(81,58,9,4),(82,378,4,3),(83,378,8,4),(84,406,7,2),(85,406,8,3),(86,6,2,1),(87,6,6,2),(88,6,10,3),(89,6,8,4),(90,175,3,1),(91,175,4,2),(92,175,10,3),(93,175,7,4),(94,241,6,2),(95,241,9,3),(96,241,10,4),(97,423,6,3),(98,423,8,4),(99,292,8,2),(100,292,10,3),(101,170,2,1),(102,170,1,2),(103,170,4,3),(104,170,4,4),(105,170,5,3),(106,378,8,2),(107,378,8,3),(108,375,10,3),(109,375,3,4),(110,435,10,3),(111,435,1,4),(112,213,2,4),(113,241,10,3),(114,241,4,4),(115,115,4,3),(116,115,4,4),(117,102,1,1),(118,102,1,2),(119,102,8,3),(120,102,4,4),(121,305,3,1),(122,305,1,2),(123,305,8,3),(124,305,3,4),(125,170,1,1),(126,170,1,2),(127,170,3,3),(128,170,1,4),(129,32,4,1),(130,32,1,2),(131,32,5,3),(132,32,10,4),(133,2,2,1),(134,2,5,2),(135,2,5,3),(136,2,8,4),(137,29,4,1),(138,29,1,2),(139,29,4,3),(140,29,6,4),(141,383,5,2),(142,383,8,3),(143,383,10,4),(144,269,6,2),(145,269,8,3),(146,269,10,4),(147,384,5,2),(148,384,8,3),(149,384,10,4),(150,173,7,2),(151,173,9,3),(152,173,10,4),(153,116,2,4),(154,130,10,3),(155,130,3,4),(156,435,10,3),(157,435,4,4),(158,6,8,2),(159,6,10,3),(160,6,4,4),(161,250,7,3),(162,250,9,4),(163,248,8,4),(164,3,6,3),(165,3,9,4),(166,213,8,4),(167,323,6,3),(168,323,5,4),(169,158,2,1),(170,158,2,2),(171,170,6,2),(172,241,9,3),(173,423,6,2),(174,423,10,3),(175,423,6,4),(176,292,7,2),(177,292,10,3),(178,292,6,4),(179,319,8,2),(180,319,10,3),(181,319,6,4),(182,19,10,3),(183,435,6,2),(184,435,8,3),(185,170,7,3),(186,78,7,2),(187,434,5,1),(188,434,6,2),(189,434,8,3),(190,434,5,4),(191,3,6,1),(192,3,6,2),(193,3,8,3),(194,3,9,4),(195,45,3,1),(196,45,6,2),(197,45,8,3),(198,45,3,4),(199,435,3,1),(200,435,6,2),(201,435,8,3),(202,435,3,4),(203,389,2,1),(204,389,2,2),(205,389,2,3),(206,389,2,4),(207,269,5,1),(208,269,5,2),(209,269,8,3),(210,269,6,4),(211,198,4,1),(212,198,4,2),(213,198,6,3),(214,198,4,4),(215,214,6,1),(216,214,6,2),(217,214,8,3),(218,214,8,4),(219,214,3,3),(220,173,5,1),(221,173,5,3),(222,323,4,3),(223,404,4,2),(224,404,3,3),(225,32,2,1),(226,32,2,2),(227,32,3,3),(228,32,2,4),(229,6,4,1),(230,6,6,2),(231,6,7,3),(232,6,6,4),(233,45,3,1),(234,45,6,2),(235,45,7,3),(236,45,7,4),(237,138,3,1),(238,138,5,2),(239,138,7,3),(240,138,8,4),(241,198,4,1),(242,198,2,2),(243,198,6,3),(244,198,5,4),(245,434,4,1),(246,434,4,2),(247,434,6,3),(248,434,6,4),(249,2,4,1),(250,2,2,2),(251,2,10,3),(252,2,3,4),(253,32,1,1),(254,32,2,2),(255,32,2,3),(256,32,2,4),(257,31,4,3),(258,31,7,4),(259,6,7,1),(260,6,8,3),(261,6,10,4),(262,268,5,1),(263,268,8,3),(264,268,10,4),(265,95,3,2),(266,95,6,3),(267,95,10,4),(268,20,6,3),(269,20,8,4),(270,98,8,1),(271,98,6,2),(272,98,8,3),(273,98,9,4),(274,3,2,4),(275,375,3,4),(276,248,7,4),(277,378,3,1),(278,378,1,2),(279,378,4,3),(280,378,4,4),(281,114,4,3),(282,119,1,1),(283,119,6,2),(284,119,10,3),(285,119,8,4),(286,357,5,1),(287,357,6,2),(288,357,8,3),(289,357,10,4),(290,170,10,3),(291,170,1,4),(292,157,1,1),(293,157,1,2),(294,157,1,3),(295,157,1,4),(296,114,3,3),(297,58,2,1),(298,58,5,2),(299,58,10,3),(300,58,10,4),(301,105,1,1),(302,105,2,2),(303,105,5,3),(304,105,7,4),(305,405,5,1),(306,405,7,3),(307,114,4,4),(308,435,9,3),(309,6,9,3),(310,3,6,3),(311,222,5,3),(312,310,3,2),(313,135,7,3),(314,79,4,1),(315,79,7,3),(316,79,7,4),(317,98,6,1),(318,98,8,3),(319,3,8,4),(320,237,8,3),(321,237,10,4),(322,227,9,1),(323,45,2,1),(324,45,2,2),(325,45,5,3),(326,45,6,4),(327,151,5,1),(328,151,4,2),(329,151,9,3),(330,151,10,4),(331,177,1,1),(332,177,2,2),(333,177,4,3),(334,177,6,4),(335,6,5,1),(336,6,8,2),(337,6,8,3),(338,6,10,4),(339,173,7,1),(340,173,8,2),(341,173,10,3),(342,173,10,4),(343,3,4,1),(344,3,5,2),(345,3,8,3),(346,3,9,4),(347,170,8,4),(348,170,6,3),(349,170,8,4),(350,31,10,3),(351,49,10,3),(352,378,5,1),(353,378,6,2),(354,378,7,3),(355,378,8,4),(356,269,7,2),(357,269,8,3),(358,269,9,4),(359,122,2,3),(360,122,2,4),(361,6,6,3),(362,6,9,4),(363,378,5,1),(364,378,7,2),(365,378,9,3),(366,378,10,4),(367,194,1,1),(368,194,1,2),(369,194,1,3),(370,194,3,4),(371,383,3,1),(372,383,3,2),(373,383,9,3),(374,383,9,4),(375,236,3,4),(376,407,5,2),(377,407,8,3),(378,407,10,4),(379,98,5,2),(380,98,7,3),(381,98,9,4),(382,307,4,2),(383,307,7,3),(384,307,9,4),(385,79,7,3),(386,8,7,4),(387,173,7,3),(388,173,9,4),(389,6,5,2),(390,6,7,3),(391,6,10,4),(392,323,4,2),(393,323,8,3),(394,323,9,4),(395,303,7,3),(396,170,8,3),(397,260,8,3),(398,42,4,3),(399,42,5,4),(400,157,7,3),(401,157,1,4),(402,3,5,2),(403,120,4,1),(404,120,6,4),(405,151,5,4),(406,241,6,4),(407,269,7,3),(408,269,7,4);
/*!40000 ALTER TABLE `peligrosidad_colonias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `peligrosidad_madrugada`
--

DROP TABLE IF EXISTS `peligrosidad_madrugada`;
/*!50001 DROP VIEW IF EXISTS `peligrosidad_madrugada`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `peligrosidad_madrugada` AS SELECT 
 1 AS `colonia`,
 1 AS `periodo`,
 1 AS `calificacion_periodo`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `peligrosidad_mañana`
--

DROP TABLE IF EXISTS `peligrosidad_mañana`;
/*!50001 DROP VIEW IF EXISTS `peligrosidad_mañana`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `peligrosidad_mañana` AS SELECT 
 1 AS `colonia`,
 1 AS `periodo`,
 1 AS `calificacion_periodo`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `peligrosidad_noche`
--

DROP TABLE IF EXISTS `peligrosidad_noche`;
/*!50001 DROP VIEW IF EXISTS `peligrosidad_noche`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `peligrosidad_noche` AS SELECT 
 1 AS `colonia`,
 1 AS `periodo`,
 1 AS `calificacion_periodo`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `peligrosidad_tarde`
--

DROP TABLE IF EXISTS `peligrosidad_tarde`;
/*!50001 DROP VIEW IF EXISTS `peligrosidad_tarde`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `peligrosidad_tarde` AS SELECT 
 1 AS `colonia`,
 1 AS `periodo`,
 1 AS `calificacion_periodo`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `periodo`
--

DROP TABLE IF EXISTS `periodo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `periodo` (
  `id_periodo` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  `hora_inicio` varchar(45) DEFAULT NULL,
  `hora_fin` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_periodo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periodo`
--

LOCK TABLES `periodo` WRITE;
/*!40000 ALTER TABLE `periodo` DISABLE KEYS */;
INSERT INTO `periodo` VALUES (1,'mañana','07:00','12:00'),(2,'tarde','12:00','19:00'),(3,'noche','19:00','24:00'),(4,'madrugada','24:00','07:00');
/*!40000 ALTER TABLE `periodo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `resumen_peligrosidad`
--

DROP TABLE IF EXISTS `resumen_peligrosidad`;
/*!50001 DROP VIEW IF EXISTS `resumen_peligrosidad`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `resumen_peligrosidad` AS SELECT 
 1 AS `colonia`,
 1 AS `periodo`,
 1 AS `calificacion_periodo`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'proteczac'
--

--
-- Final view structure for view `peligrosidad_madrugada`
--

/*!50001 DROP VIEW IF EXISTS `peligrosidad_madrugada`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `peligrosidad_madrugada` AS select `c`.`nombre_colonia` AS `colonia`,`p`.`descripcion` AS `periodo`,sum(`pc`.`calificacion`) AS `calificacion_periodo` from ((`peligrosidad_colonias` `pc` join `colonia` `c` on((`c`.`id_colonia` = `pc`.`colonia`))) join `periodo` `p` on((`p`.`id_periodo` = `pc`.`periodo`))) where (`p`.`descripcion` = 'madrugada') group by `pc`.`colonia`,`pc`.`periodo` order by `c`.`nombre_colonia` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `peligrosidad_mañana`
--

/*!50001 DROP VIEW IF EXISTS `peligrosidad_mañana`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `peligrosidad_mañana` AS select `c`.`nombre_colonia` AS `colonia`,`p`.`descripcion` AS `periodo`,sum(`pc`.`calificacion`) AS `calificacion_periodo` from ((`peligrosidad_colonias` `pc` join `colonia` `c` on((`c`.`id_colonia` = `pc`.`colonia`))) join `periodo` `p` on((`p`.`id_periodo` = `pc`.`periodo`))) where (`p`.`descripcion` = 'mañana') group by `pc`.`colonia`,`pc`.`periodo` order by `c`.`nombre_colonia` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `peligrosidad_noche`
--

/*!50001 DROP VIEW IF EXISTS `peligrosidad_noche`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `peligrosidad_noche` AS select `c`.`nombre_colonia` AS `colonia`,`p`.`descripcion` AS `periodo`,sum(`pc`.`calificacion`) AS `calificacion_periodo` from ((`peligrosidad_colonias` `pc` join `colonia` `c` on((`c`.`id_colonia` = `pc`.`colonia`))) join `periodo` `p` on((`p`.`id_periodo` = `pc`.`periodo`))) where (`p`.`descripcion` = 'noche') group by `pc`.`colonia`,`pc`.`periodo` order by `c`.`nombre_colonia` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `peligrosidad_tarde`
--

/*!50001 DROP VIEW IF EXISTS `peligrosidad_tarde`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `peligrosidad_tarde` AS select `c`.`nombre_colonia` AS `colonia`,`p`.`descripcion` AS `periodo`,sum(`pc`.`calificacion`) AS `calificacion_periodo` from ((`peligrosidad_colonias` `pc` join `colonia` `c` on((`c`.`id_colonia` = `pc`.`colonia`))) join `periodo` `p` on((`p`.`id_periodo` = `pc`.`periodo`))) where (`p`.`descripcion` = 'tarde') group by `pc`.`colonia`,`pc`.`periodo` order by `c`.`nombre_colonia` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `resumen_peligrosidad`
--

/*!50001 DROP VIEW IF EXISTS `resumen_peligrosidad`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `resumen_peligrosidad` AS select `c`.`nombre_colonia` AS `colonia`,`p`.`descripcion` AS `periodo`,sum(`pc`.`calificacion`) AS `calificacion_periodo` from ((`peligrosidad_colonias` `pc` join `colonia` `c` on((`c`.`id_colonia` = `pc`.`colonia`))) join `periodo` `p` on((`p`.`id_periodo` = `pc`.`periodo`))) group by `pc`.`colonia`,`pc`.`periodo` order by `c`.`nombre_colonia` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-01 11:06:51
