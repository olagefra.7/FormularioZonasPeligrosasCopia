package mx.edu.uaz.accesoDatos;

import com.vaadin.ui.Notification;
import mx.edu.uaz.modelo.Colonia;
import mx.edu.uaz.modelo.Municipio;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class ADMunicipio {
    public List<Municipio> obtenerTodosMunicipios(){
        SqlSession session = Config.abreSesion();
        List<Municipio> lista = null;
        try{
            lista = session.selectList("obtenerTodosMunicipios");
        }catch (Exception e){
            Notification.show("No se puede mostrar las colonias",Notification.Type.ERROR_MESSAGE);
            e.printStackTrace();
        }finally {
            session.close();
        }
        return lista;
    }

}