package mx.edu.uaz.accesoDatos;

import com.vaadin.ui.Notification;
import mx.edu.uaz.modelo.Colonia;
import mx.edu.uaz.modelo.Municipio;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class ADColonia {
    public List<Colonia> obtenerTodasColonias(){
        SqlSession session = Config.abreSesion();
        List<Colonia> lista = null;
        try{
            lista = session.selectList("obtenerTodasColonias");
        }catch (Exception e){
            Notification.show("No se puede mostrar las colonias",Notification.Type.ERROR_MESSAGE);
            e.printStackTrace();
        }finally {
            session.close();
        }
        return lista;
    }
    public List<Colonia> obtenerColoniasMunicipio(String mun){
        SqlSession session = Config.abreSesion();
        List<Colonia> lista = null;
        try{
            lista = session.selectList("obtenerColoniasMunicipio", mun);
        }catch (Exception e){
            Notification.show("No se puede mostrar las colonias",Notification.Type.ERROR_MESSAGE);
            e.printStackTrace();
        }finally {
            session.close();
        }
        return lista;
    }
}