package mx.edu.uaz.accesoDatos;

import com.vaadin.ui.Notification;
import mx.edu.uaz.modelo.Colonia;
import mx.edu.uaz.modelo.IndicePeligro;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class ADIndicePeligro {
    public boolean agregaIndicePeligro(IndicePeligro indice){
        SqlSession sesion = Config.abreSesion();
        boolean ok = false;

        try {
            sesion.insert("agregaIndice", indice);
            sesion.commit();
            ok = true;
        }catch (Exception e){
            Notification.show("No se pudo registrar tu opinión");
        }finally {
            sesion.close();
        }
        return ok;
    }
}