package mx.edu.uaz.vista;

import com.vaadin.data.Binder;
import com.vaadin.data.HasValue;
import com.vaadin.ui.*;
import mx.edu.uaz.accesoDatos.ADColonia;
import mx.edu.uaz.accesoDatos.ADIndicePeligro;
import mx.edu.uaz.modelo.Colonia;
import mx.edu.uaz.modelo.IndicePeligro;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class ControladorForm extends FormularioDesigner{
    private Binder<Colonia> binderColonia;
    private ADColonia adColonia;
    private List coloniasZacatecas;
    private List coloniasGuadalupe;

    public ControladorForm(){
        adColonia = new ADColonia();
        coloniasZacatecas = adColonia.obtenerColoniasMunicipio("Zacatecas");
        coloniasGuadalupe = adColonia.obtenerColoniasMunicipio("Guadalupe");
        habilitaChecks(false);
        habilitaCombosColonias(false);
        habilitaCombosCalificaciones(false);
        setPlaceholder();
        llenarComboBox();
        enviarBoton();
    }

    public void enviarBoton(){
        btnEnviar.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                //if (binder.validate().isOk()) {
                try {
                    if (cbo_col1.getValue() != null) {
                        if (check_manana1.getValue()) {
                            IndicePeligro ind = new IndicePeligro();
                            ind.setCalificacion(cbo_cal_m1.getValue());
                            ind.setColonia(cbo_col1.getValue().getIdColonia());
                            ind.setHorario(1);
                            insertaIndice(ind);
                        }
                        if (check_tarde1.getValue() ) {
                            IndicePeligro ind = new IndicePeligro();
                            ind.setCalificacion(cbo_cal_t1.getValue());
                            ind.setColonia(cbo_col1.getValue().getIdColonia());
                            ind.setHorario(2);
                            insertaIndice(ind);
                        }
                        if (check_noche1.getValue() ) {
                            IndicePeligro ind = new IndicePeligro();
                            ind.setCalificacion(cbo_cal_n1.getValue());
                            ind.setColonia(cbo_col1.getValue().getIdColonia());
                            ind.setHorario(3);
                            insertaIndice(ind);
                        }
                        if (check_madrugada1.getValue()) {
                            IndicePeligro ind = new IndicePeligro();
                            ind.setCalificacion(cbo_cal_mad1.getValue());
                            ind.setColonia(cbo_col1.getValue().getIdColonia());
                            ind.setHorario(4);
                            insertaIndice(ind);
                        }
                    }
                    if (cbo_col2.getValue() != null) {
                        if (check_manana2.getValue()) {
                            IndicePeligro ind = new IndicePeligro();
                            ind.setCalificacion(cbo_cal_m2.getValue());
                            ind.setColonia(cbo_col2.getValue().getIdColonia());
                            ind.setHorario(1);
                            insertaIndice(ind);
                        }
                        if (check_tarde2.getValue() ) {
                            IndicePeligro ind = new IndicePeligro();
                            ind.setCalificacion(cbo_cal_t2.getValue());
                            ind.setColonia(cbo_col2.getValue().getIdColonia());
                            ind.setHorario(2);
                            insertaIndice(ind);
                        }
                        if (check_noche2.getValue() ) {
                            IndicePeligro ind = new IndicePeligro();
                            ind.setCalificacion(cbo_cal_n2.getValue());
                            ind.setColonia(cbo_col2.getValue().getIdColonia());
                            ind.setHorario(3);
                            insertaIndice(ind);
                        }
                        if (check_madrugada2.getValue() && cbo_cal_mad2.getValue() != null) {
                            IndicePeligro ind = new IndicePeligro();
                            ind.setCalificacion(cbo_cal_mad2.getValue());
                            ind.setColonia(cbo_col2.getValue().getIdColonia());
                            ind.setHorario(4);
                            insertaIndice(ind);
                        }
                    }
                    if (cbo_col3.getValue() != null) {
                        if (check_manana3.getValue() ) {
                            IndicePeligro ind = new IndicePeligro();
                            ind.setCalificacion(cbo_cal_m3.getValue());
                            ind.setColonia(cbo_col3.getValue().getIdColonia());
                            ind.setHorario(1);
                            insertaIndice(ind);
                        }
                        if (check_tarde3.getValue() ) {
                            IndicePeligro ind = new IndicePeligro();
                            ind.setCalificacion(cbo_cal_t3.getValue());
                            ind.setColonia(cbo_col3.getValue().getIdColonia());
                            ind.setHorario(2);
                            insertaIndice(ind);
                        }
                        if (check_noche3.getValue()) {
                            IndicePeligro ind = new IndicePeligro();
                            ind.setCalificacion(cbo_cal_n3.getValue());
                            ind.setColonia(cbo_col3.getValue().getIdColonia());
                            ind.setHorario(3);
                            insertaIndice(ind);
                        }
                        if (check_madrugada3.getValue()) {
                            IndicePeligro ind = new IndicePeligro();
                            ind.setCalificacion(cbo_cal_mad3.getValue());
                            ind.setColonia(cbo_col3.getValue().getIdColonia());
                            ind.setHorario(4);
                            insertaIndice(ind);
                        }
                    }
                    if (cbo_col4.getValue() != null) {
                        if (check_manana4.getValue() ) {
                            IndicePeligro ind = new IndicePeligro();
                            ind.setCalificacion(cbo_cal_m4.getValue());
                            ind.setColonia(cbo_col4.getValue().getIdColonia());
                            ind.setHorario(1);
                            insertaIndice(ind);
                        }
                        if (check_tarde4.getValue() ) {
                            IndicePeligro ind = new IndicePeligro();
                            ind.setCalificacion(cbo_cal_t4.getValue());
                            ind.setColonia(cbo_col4.getValue().getIdColonia());
                            ind.setHorario(2);
                            insertaIndice(ind);
                        }
                        if (check_noche4.getValue()) {
                            IndicePeligro ind = new IndicePeligro();
                            ind.setCalificacion(cbo_cal_n4.getValue());
                            ind.setColonia(cbo_col4.getValue().getIdColonia());
                            ind.setHorario(3);
                            insertaIndice(ind);
                        }
                        if (check_madrugada4.getValue()) {
                            IndicePeligro ind = new IndicePeligro();
                            ind.setCalificacion(cbo_cal_mad4.getValue());
                            ind.setColonia(cbo_col4.getValue().getIdColonia());
                            ind.setHorario(4);
                            insertaIndice(ind);
                        }
                    }

                    UI.getCurrent().setContent(new Agradecimiento());
                }catch(java.lang.NullPointerException e){
                    Notification.show("Por favor completa los campos que seleccionaste");
                }

            }
        });
    }

    public boolean insertaIndice(IndicePeligro ind){
        ADIndicePeligro adIndice = new ADIndicePeligro();
        boolean ok = false;
        ok = adIndice.agregaIndicePeligro(ind);
        return ok;
    }

    private void llenarComboBox(){
        adColonia = new ADColonia();


        List<String> municipios = new ArrayList<>();
        municipios.add("Zacatecas");
        municipios.add("Guadalupe");
        cbo_mun1.setItems(municipios);
        cbo_mun1.addValueChangeListener(new CboMunicipioListener(1));
        cbo_mun2.setItems(municipios);
        cbo_mun2.addValueChangeListener(new CboMunicipioListener(2));
        cbo_mun3.setItems(municipios);
        cbo_mun3.addValueChangeListener(new CboMunicipioListener(3));
        cbo_mun4.setItems(municipios);
        cbo_mun4.addValueChangeListener(new CboMunicipioListener(4));


        HasValue.ValueChangeListener listenerColonia = new CboColoniaListener();
        cbo_col1.addValueChangeListener(listenerColonia);
        cbo_col2.addValueChangeListener(listenerColonia);
        cbo_col3.addValueChangeListener(listenerColonia);
        cbo_col4.addValueChangeListener(listenerColonia);

        HasValue.ValueChangeListener listenerChecks = new ChecksListener();
        check_manana1.addValueChangeListener(listenerChecks);
        check_manana2.addValueChangeListener(listenerChecks);
        check_manana3.addValueChangeListener(listenerChecks);
        check_manana4.addValueChangeListener(listenerChecks);
        check_tarde1.addValueChangeListener(listenerChecks);
        check_tarde2.addValueChangeListener(listenerChecks);
        check_tarde3.addValueChangeListener(listenerChecks);
        check_tarde4.addValueChangeListener(listenerChecks);
        check_noche1.addValueChangeListener(listenerChecks);
        check_noche2.addValueChangeListener(listenerChecks);
        check_noche3.addValueChangeListener(listenerChecks);
        check_noche4.addValueChangeListener(listenerChecks);
        check_madrugada1.addValueChangeListener(listenerChecks);
        check_madrugada2.addValueChangeListener(listenerChecks);
        check_madrugada3.addValueChangeListener(listenerChecks);
        check_madrugada4.addValueChangeListener(listenerChecks);

        List<Integer> numeros = new ArrayList<Integer>();
        for(int i = 1; i <= 10; i++){
            numeros.add(i);
        }
        cbo_cal_m1.setItems(numeros);
        cbo_cal_m2.setItems(numeros);
        cbo_cal_m3.setItems(numeros);
        cbo_cal_m4.setItems(numeros);
        cbo_cal_n1.setItems(numeros);
        cbo_cal_n2.setItems(numeros);
        cbo_cal_n3.setItems(numeros);
        cbo_cal_n4.setItems(numeros);
        cbo_cal_t1.setItems(numeros);
        cbo_cal_t2.setItems(numeros);
        cbo_cal_t3.setItems(numeros);
        cbo_cal_t4.setItems(numeros);
        cbo_cal_mad1.setItems(numeros);
        cbo_cal_mad2.setItems(numeros);
        cbo_cal_mad3.setItems(numeros);
        cbo_cal_mad4.setItems(numeros);



    }



    public void habilitaCombosColonias(boolean habilita){
        cbo_col1.setEnabled(habilita);
        cbo_col2.setEnabled(habilita);
        cbo_col3.setEnabled(habilita);
        cbo_col4.setEnabled(habilita);
    }
    public void habilitaChecks(boolean habilitado){
        check_manana1.setEnabled(habilitado);
        check_tarde1.setEnabled(habilitado);
        check_noche1.setEnabled(habilitado);
        check_madrugada1.setEnabled(habilitado);

        check_manana2.setEnabled(habilitado);
        check_tarde2.setEnabled(habilitado);
        check_noche2.setEnabled(habilitado);
        check_madrugada2.setEnabled(habilitado);

        check_manana3.setEnabled(habilitado);
        check_tarde3.setEnabled(habilitado);
        check_noche3.setEnabled(habilitado);
        check_madrugada3.setEnabled(habilitado);

        check_manana4.setEnabled(habilitado);
        check_tarde4.setEnabled(habilitado);
        check_noche4.setEnabled(habilitado);
        check_madrugada4.setEnabled(habilitado);
    }
    public void habilitaCombosCalificaciones(boolean habilitado){
        cbo_cal_m1.setEnabled(habilitado);
        cbo_cal_m2.setEnabled(habilitado);
        cbo_cal_m3.setEnabled(habilitado);
        cbo_cal_m4.setEnabled(habilitado);
        cbo_cal_n1.setEnabled(habilitado);
        cbo_cal_n2.setEnabled(habilitado);
        cbo_cal_n3.setEnabled(habilitado);
        cbo_cal_n4.setEnabled(habilitado);
        cbo_cal_t1.setEnabled(habilitado);
        cbo_cal_t2.setEnabled(habilitado);
        cbo_cal_t3.setEnabled(habilitado);
        cbo_cal_t4.setEnabled(habilitado);
        cbo_cal_mad1.setEnabled(habilitado);
        cbo_cal_mad2.setEnabled(habilitado);
        cbo_cal_mad3.setEnabled(habilitado);
        cbo_cal_mad4.setEnabled(habilitado);
    }

   /*public void ponerPlaceholder(){
        cbo_cal_m1.setEmptySelectionCaption("Nivel 1-10");
        cbo_cal_m2.setEmptySelectionCaption("Nivel 1-10");
        cbo_cal_m3.setEmptySelectionCaption("Nivel 1-10");
        cbo_cal_m4.setEmptySelectionCaption("Nivel 1-10");
        cbo_cal_n1.setEmptySelectionCaption("Nivel 1-10");
        cbo_cal_n2.setEmptySelectionCaption("Nivel 1-10");
        cbo_cal_n3.setEmptySelectionCaption("Nivel 1-10");
        cbo_cal_n4.setEmptySelectionCaption("Nivel 1-10");
        cbo_cal_t1.setEmptySelectionCaption("Nivel 1-10");
        cbo_cal_t2.setEmptySelectionCaption("Nivel 1-10");
        cbo_cal_t3.setEmptySelectionCaption("Nivel 1-10");
        cbo_cal_t4.setEmptySelectionCaption("Nivel 1-10");
        cbo_cal_mad1.setEmptySelectionCaption("Nivel 1-10");
        cbo_cal_mad2.setEmptySelectionCaption("Nivel 1-10");
        cbo_cal_mad3.setEmptySelectionCaption("Nivel 1-10");
        cbo_cal_mad4.setEmptySelectionCaption("Nivel 1-10");

       /*cbo_cal_m1.setSelectedItem(0);
       cbo_cal_m2.setSelectedItem(0);
       cbo_cal_m3.setSelectedItem(0);
       cbo_cal_m4.setSelectedItem(0);
       cbo_cal_n1.setSelectedItem(0);
       cbo_cal_n2.setSelectedItem(0);
       cbo_cal_n3.setSelectedItem(0);
       cbo_cal_n4.setSelectedItem(0);
       cbo_cal_t1.setSelectedItem(0);
       cbo_cal_t2.setSelectedItem(0);
       cbo_cal_t3.setSelectedItem(0);
       cbo_cal_t4.setSelectedItem(0);
       cbo_cal_mad1.setSelectedItem(0);
       cbo_cal_mad2.setSelectedItem(0);
       cbo_cal_mad3.setSelectedItem(0);
       cbo_cal_mad4.setSelectedItem(0);
    }*/

   public void setPlaceholder(){
       cbo_col1.setPlaceholder("Buscar");
       cbo_col2.setPlaceholder("Buscar");
       cbo_col3.setPlaceholder("Buscar");
       cbo_col4.setPlaceholder("Buscar");
   }

    class ChecksListener implements CheckBox.ValueChangeListener{

        @Override
        public void valueChange(HasValue.ValueChangeEvent event) {
            if (event.getComponent() == check_manana1) {
                cbo_cal_m1.setEnabled(check_manana1.getValue());
            }
            if (event.getComponent() == check_manana2) {
                cbo_cal_m2.setEnabled(check_manana2.getValue());
            }
            if (event.getComponent() == check_manana3) {
                cbo_cal_m3.setEnabled(check_manana3.getValue());
            }
            if (event.getComponent() == check_manana4) {
                cbo_cal_m4.setEnabled(check_manana4.getValue());
            }

            if (event.getComponent() == check_tarde1) {
                cbo_cal_t1.setEnabled(check_tarde1.getValue());
            }
            if (event.getComponent() == check_tarde2) {
                cbo_cal_t2.setEnabled(check_tarde2.getValue());
            }
            if (event.getComponent() == check_tarde3) {
                cbo_cal_t3.setEnabled(check_tarde3.getValue());
            }
            if (event.getComponent() == check_tarde4) {
                cbo_cal_t4.setEnabled(check_tarde4.getValue());
            }

            if (event.getComponent() == check_noche1) {
                cbo_cal_n1.setEnabled(check_noche1.getValue());
            }
            if (event.getComponent() == check_noche2) {
                cbo_cal_n2.setEnabled(check_noche2.getValue());
            }
            if (event.getComponent() == check_noche3) {
                cbo_cal_n3.setEnabled(check_noche3.getValue());
            }
            if (event.getComponent() == check_noche4) {
                cbo_cal_n4.setEnabled(check_noche4.getValue());
            }

            if (event.getComponent() == check_madrugada1) {
                cbo_cal_mad1.setEnabled(check_madrugada1.getValue());
            }
            if (event.getComponent() == check_madrugada2) {
                cbo_cal_mad2.setEnabled(check_madrugada2.getValue());
            }
            if (event.getComponent() == check_madrugada3) {
                cbo_cal_mad3.setEnabled(check_madrugada3.getValue());
            }
            if (event.getComponent() == check_madrugada4) {
                cbo_cal_mad4.setEnabled(check_madrugada4.getValue());
            }
        }
    }

    class CboColoniaListener implements ComboBox.ValueChangeListener {

        @Override
        public void valueChange(HasValue.ValueChangeEvent event) {
            if (event.getComponent() == cbo_col1){
                check_manana1.setEnabled(true);
                check_tarde1.setEnabled(true);
                check_noche1.setEnabled(true);
                check_madrugada1.setEnabled(true);
            }
            if (event.getComponent() == cbo_col2){
                check_manana2.setEnabled(true);
                check_tarde2.setEnabled(true);
                check_noche2.setEnabled(true);
                check_madrugada2.setEnabled(true);
            }
            if (event.getComponent() == cbo_col3){
                check_manana3.setEnabled(true);
                check_tarde3.setEnabled(true);
                check_noche3.setEnabled(true);
                check_madrugada3.setEnabled(true);
            }
            if (event.getComponent() == cbo_col4){
                check_manana4.setEnabled(true);
                check_tarde4.setEnabled(true);
                check_noche4.setEnabled(true);
                check_madrugada4.setEnabled(true);
            }
        }
    }


    class CboMunicipioListener implements HasValue.ValueChangeListener {
        private int numCombo;
        public CboMunicipioListener(int numCombo){
            this.numCombo = numCombo;
        }

        @Override
        public void valueChange(HasValue.ValueChangeEvent valueChangeEvent) {

            switch (numCombo){
                case 1:
                    if (cbo_mun1.getValue().equals("Zacatecas"))
                        cbo_col1.setItems(coloniasZacatecas);
                    else
                        cbo_col1.setItems(coloniasGuadalupe);
                    cbo_col1.setEnabled(true);
                    break;
                case 2:
                    if (cbo_mun2.getValue().equals("Zacatecas"))
                        cbo_col2.setItems(coloniasZacatecas);
                    else
                        cbo_col2.setItems(coloniasGuadalupe);
                    cbo_col2.setEnabled(true);
                    break;
                case 3:
                    if (cbo_mun3.getValue().equals("Zacatecas"))
                        cbo_col3.setItems(coloniasZacatecas);
                    else
                        cbo_col3.setItems(coloniasGuadalupe);
                    cbo_col3.setEnabled(true);
                    break;
                case 4:
                    if (cbo_mun4.getValue().equals("Zacatecas"))
                        cbo_col4.setItems(coloniasZacatecas);
                    else
                        cbo_col4.setItems(coloniasGuadalupe);
                    cbo_col4.setEnabled(true);
                    break;
            }
        }
    }


}

