package mx.edu.uaz.vista;

import com.vaadin.server.ExternalResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Link;
import com.vaadin.ui.UI;

public class Agradecimiento extends CustomLayout {
    private Link blog;

    public Agradecimiento(){
        setResponsive(true);
        setWidth("100%");
        setHeight("100%");
        setTemplateName("agradecimiento");

        blog = new Link("Click aquí para vistar el blog",
                new ExternalResource("https://goombadiaries.blogspot.com"));
        addComponent(blog,"link-blog");
    }

}
