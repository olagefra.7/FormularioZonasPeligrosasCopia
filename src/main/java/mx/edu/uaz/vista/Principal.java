package mx.edu.uaz.vista;

import com.vaadin.ui.Button;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.UI;

public class Principal extends CustomLayout {
    private Button btnContinuar;

    public Principal(){
        setResponsive(true);
        setWidth("100%");
        setHeight("100%");
        setTemplateName("principal");


        btnContinuar = new Button("Continuar");
        btnContinuar.setStyleName("btn-continuar");
        btnContinuar.addClickListener(clickEvent -> {
            UI.getCurrent().setContent(new ControladorForm());
        });
        addComponent(btnContinuar,"btn-continuar");
    }

}
