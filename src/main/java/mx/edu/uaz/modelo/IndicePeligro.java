package mx.edu.uaz.modelo;

public class IndicePeligro {
    private int colonia;
    private int calificacion;
    private int horario;

    public int getColonia() {
        return colonia;
    }

    public void setColonia(int colonia) {
        this.colonia = colonia;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    public int getHorario() {
        return horario;
    }

    public void setHorario(int horario) {
        this.horario = horario;
    }
}
